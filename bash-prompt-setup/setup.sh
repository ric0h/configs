#!/bin/bash
sudo apt install git -y
git clone https://github.com/magicmonty/bash-git-prompt.git ~/.bash-git-prompt
git clone git@gitlab.com:ric0h/configs.git ~/Desktop/git_prompt_config

cp ~/Desktop/git_prompt_config/bash-prompt-setup/.git-prompt-colors.sh ~/
mkdir ~/sbin && cp ~/Desktop/git_prompt_config/bash-prompt-setup/vcprompt.py ~/sbin

cat <<GIT_PROMPT >>~/.bashrc
if [ -f "$HOME/.bash-git-prompt/gitprompt.sh" ]; then
    GIT_PROMPT_ONLY_IN_REPO=1
    GIT_PROMPT_THEME=Custom
    source $HOME/.bash-git-prompt/gitprompt.sh
fi

# Kustome-Bash-Git-Prompt
export TERM=xterm-color
# export GREP_OPTIONS='--color=auto' GREP_COLOR='1;32'
export CLICOLOR=1
export LSCOLORS=ExFxCxDxBxegedabagacad
export COLOR_NC='\e[0m' # No Color
export COLOR_WHITE='\e[1;37m'
export COLOR_BLACK='\e[0;30m'
export COLOR_BLUE='\e[0;34m'
export COLOR_LIGHT_BLUE='\e[1;34m'
export COLOR_GREEN='\e[0;32m'
export COLOR_LIGHT_GREEN='\e[1;32m'
export COLOR_CYAN='\e[0;36m'
export COLOR_LIGHT_CYAN='\e[1;36m'
export COLOR_RED='\e[0;31m'
export COLOR_LIGHT_RED='\e[1;31m'
export COLOR_PURPLE='\e[0;35m'
export COLOR_LIGHT_PURPLE='\e[1;35m'
export COLOR_BROWN='\e[0;33m'
export COLOR_YELLOW='\e[1;33m'
export COLOR_GRAY='\e[0;30m'
export COLOR_LIGHT_GRAY='\e[0;37m'

alias vcprompt="~/sbin/vcprompt.py"

if [ -f ~/sbin/vcprompt.py ]; then
    UC=$COLOR_LIGHT_GREEN
    [ $UID -eq "0" ] && UC=$COLOR_RED  # root's color

    PS1="$TITLEBAR\[${UC}\]\u:\[${COLOR_LIGHT_BLUE}\][\w] \[${COLOR_LIGHT_RED}\]\$(vcprompt) \n\[${COLOR_LIGHT_GREEN}\]→\[${COLOR_NC}\] " 
fi
GIT_PROMPT

source ~/.bashrc
rm -rf ~/Desktop/git_prompt_config
unset GREP_OPTIONS
