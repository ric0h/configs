#!/bin/bash

## VERSIONS
GOLANG_VERSION=1.13.8
WORKING_DIR=$HOME/Desktop/go_version
DOWNLOAD_URL="https://dl.google.com/go/go$GOLANG_VERSION.linux-amd64.tar.gz"

## Install
read -p "Enter the version of go you would like to install, default version is $GOLANG_VERSION
Press enter if you want to maintain the default version:" go_version

[ ! -z $go_version ] && echo -ne "Setting go version to $go_version\r" || echo -ne "Setting default go version\r"

mkdir $WORKING_DIR &> /dev/null
cd $WORKING_DIR
# echo -ne "Downloading go package ...\r"
# wget $DOWNLOAD_URL &> /dev/null
# if [ $? = 0 ]; then echo -ne "Go package downloaded in '$WORKING_DIR'/r"; sleep 0.5;
# else echo "Error downloading package, please check the version or url of download\r"; exit

echo -ne "Extracting package to '/usr/local/'...\r"
sudo tar -C /usr/local -xzf go$GOLANG_VERSION.linux-amd64.tar.gz &> /dev/null

echo -ne "Exporting path...\r"; sleep 0.5
export PATH=$PATH:/usr/local/go/bin
echo "export PATH=$PATH:/usr/local/go/bin" >> ~/.bash_aliases
source ~/.bashrc

echo -ne "Clearing downloaded package...\r"; sleep 0.5
rm -rf go$GOLANG_VERSION.linux-amd64.tar.gz

## test go
echo -ne "Testing go...\r"; sleep 0.5
touch $WORKING_DIR/version.go
echo "package main
import fmt
func main() {
    fmt.Printf('Installed Go version $GOLANG_VERSION .'
}" > $WORKING_DIR/version.go





