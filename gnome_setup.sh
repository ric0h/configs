#!/bin/bash

## Update 
sudo apt-get update -y
sudo apt-get upgrade -y

## Gnome Tweaks
sudo apt-get install gnome-tweak-tool

## Pomodoro
sudo apt-get install gnome-shell-pomodoro

## 
sudo apt-get install chrome-gnome-shellsudo apt-get install chrome-gnome-shell

## Preferred apps
sudo apt-get install vlc
sudo apt-get install qbittorrent
sudo apt remove firefox
sudo apt remove transmission
sudo apt-get install insomnia


sudo add-apt-repository ppa:diodon-team/stable
sudo apt-get install diodon
sudo snap install slack --classic