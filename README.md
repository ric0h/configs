# configs

Storage for for configuration setting of the various things i constantly find myself rewriting, these include aliases in `.bashrc` to vscode settings among others such as scripts to setup the various technologies and services i use in linux system.

## Modifying bash prompt when in a .git repository

Adds some stats about the repo to the command prompt.  
For more details checkout [this setup](bash-prompt-setup/README.md).
