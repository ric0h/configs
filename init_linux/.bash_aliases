#!/bin/bash
# LINUX SHoRTS
alias rm="rm -I"

## apt
_installPackage() {
	local app=$1
	sudo apt-get update
	sudo apt-get install $app -y || sudo snap install $app --classic
	if [ $? -eq 0 ]; then
		chck=$(cat ~/.installed_packages | grep $app | wc -l)
		if [ "$chck" = "0" ]; then
			echo $app >>~/.installed_packages
		fi
		return 0
	fi
}
alias update="sudo apt-get update"
alias upgrade="sudo apt-get upgrade"
alias upgrades="apt list --upgradable"
alias full-upgrade="sudo apt-get full-upgrade"
alias search="apt-get search"
alias show="apt-get show"
alias install=_installPackage
alias remove="sudo apt-get remove"
alias dpkg="sudo dpkg"
alias add-key="sudo apt-key adv --keyserver keyserver.ubuntu.com --recv-keys"

# GIT SHORTS
_quickFix() {
	git rev-parse &>/dev/null
	if [ "$?" -eq 0 ]; then
		local patch_branch
		local additions
		local removals
		local rando

		additions=$(git diff | grep '^+' | wc -l)
		removals=$(git diff | grep '^-' | wc -l)
		rando=$(git describe --always --dirty=+"$additions"-"$removals")

		if [ -z "$1" ]; then
			patch_branch=patches_$rando
		else
			patch_branch=$1
		fi
		git branch --show-current >./.git/.custom_prev_branch
		git add .
		git stash
		git checkout "$patch_branch" 2>/dev/null || git checkout -b "$patch_branch"
	else
		echo "Not in git repo"
	fi
}

_doneFix() {
	git rev-parse &>/dev/null
	if [ "$?" -eq 0 ]; then
		local prev_branch
		prev_branch=$(cat ./.git/.custom_prev_branch)
		if [ -z "$prev_branch" ]; then
			echo "no quickfix history found"
		else
			echo "reverting back to $prev_branch"
			git checkout "$prev_branch"
			git stash pop
			echo "" >./.git/.custom_prev_branch
		fi
	else
		echo "Not in git repo"
	fi
}
alias clone="git clone"
alias add="git add"
alias commit="git commit"
alias pull="git pull"
alias push="git push"
alias rebase="git rebase"
alias checkout="git checkout"
alias fetch="git fetch -a"
alias branch="git branch -a"
alias status="git status"
alias log="git log"
alias log1="git log --oneline"
alias stash="git stash"
alias gdiff="git diff"
alias merge="git merge"
alias quickfix=_quickFix
alias donefix=_doneFix

# ELIXIR'S MIX ENVIRONMENT
alias mix-server="source ~/.ally.env && cd ./assets && npm install && cd .. && iex -S mix phx.server"
alias phx-server="iex -S mix phx.server"
alias resetdb="mix ecto.reset"
alias testdb="MIX_ENV=test"
alias devdb="MIX_ENV=dev"
alias prodb="MIX_ENV=prod"
alias migrate="mix ecto.migrate"
alias run-server="mix phx.server"
alias mig-gen="mix ecto.gen.migration"

# POSTGRESQL
# ANDROID
alias avdm="~/Android/Sdk/tools/bin/avdmanager"
alias emulator="~/Android/Sdk/tools/emulator"
alias avd-start="emulator -avd Nexus_5X_API_27"
alias anbox.bridge="sudo ~/Downloads/anbox-bridge-master/anbox-bridge.sh start"

# BASHRC modifiers
export PS1='${debian_chroot:+($debian_chroot)}\[\033[01;32m\]\u\[\033[00m\]:\[\033[01;34m\]\w\[\033[00m\]\$ '
HISTFILESIZE=20000
export QT_QPA_PLATFORMTHEME=gtk2
