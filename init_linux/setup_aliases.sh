#!/bin/bash

# copy aliases to /home/
mv ~/.bash_aliases ~/.bash_aliases.bak
sudo cp "$(pwd)/.bash_aliases" ~

if [ "$?" -eq 0 ]; then
	sudo chmod 555 ~/.bash_aliases
	source ~/.bash_aliases
	chck=$(cat ~/.bashrc | grep -e '^if \[ -f ~/.bash_aliases' -e '. ~/.bash_aliases')
	if [ "$chck" == "0" ]; then
		echo ".bash_aliases is loaded!"
	else
		echo "source ~/.bash_aliases" >>~/.bashrc
	fi
	source ~/.bashrc
fi
