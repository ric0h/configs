#!/bin/bash
_get_distro() {
    lsb_dist=""
    # Every system that we officially support has /etc/os-release
    if [ -r /etc/os-release ]; then
        lsb_dist="$(. /etc/os-release && echo "$ID")"
    fi
    # Returning an empty string here should be alright since the
    # case statements don't act unless you provide an actual value
    echo "$lsb_dist"
}

echo "|>-----------------------------------------------------------<|"
echo "|     Installing docker from convinence script...             |"
echo "|>------------------------------------------------------------|"

curl -fsSL https://get.docker.com -o get-docker.sh
sudo sh get-docker.sh

sudo systemctl start docker

echo "|>-------------------------------------------------------------<|"
echo "|>    Docker Successfully started and running...                |"
echo "|>-------------------------------------------------------------<|"

systemctl status docker

echo
echo
