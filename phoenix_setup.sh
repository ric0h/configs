#!/bin/bash
cd ~
git clone git@github.com:BenBera/Mtv_api.git

cd Mtv_api
export MIX_ENV=prod
mix do deps.get, compile
mix phx.gen.secret

## Set VERSIONS
DISTRO="$(lsb_release -s -c)"
NODE_VERSION=node_12.x
POSTGRESQL_VERSION=11
PHX_VERSION=1.4.10
ERLANG_VERSION=
ELIXIR_VERSION=

## ELIXIR AND ERLANG
wget https://packages.erlang-solutions.com/erlang-solutions_2.0_all.deb && sudo dpkg -i erlang-solutions_2.0_all.deb
sudo apt update
sudo apt-get install esl-erlang -y
sudo apt-get install elixir -y
rm erlang-solutions_2.0_all.deb
mix local.hex -y
mix local.rebar --force -y
mix archive.install hex phx_new $PHX_VERSION

## NODEJS
wget --quiet -O - https://deb.nodesource.com/gpgkey/nodesource.gpg.key | sudo apt-key add -
# Replace with the branch of Node.js or io.js you want to install: node_6.x, node_8.x, etc...
# The below command will set this correctly, but if lsb_release isn't available, you can set it manually:
# - For Debian distributions: jessie, sid, etc...
# - For Ubuntu distributions: xenial, bionic, etc...
# - For Debian or Ubuntu derived distributions your best option is to use the codename corresponding to the upstream release your distribution is based off. This is an advanced scenario and unsupported if your distribution is not listed as supported per earlier in this README.
echo "deb https://deb.nodesource.com/$NODE_VERSION $DISTRO main" | sudo tee /etc/apt/sources.list.d/nodesource.list
echo "deb-src https://deb.nodesource.com/$NODE_VERSION $DISTRO main" | sudo tee -a /etc/apt/sources.list.d/nodesource.list
sudo apt-get update -y
sudo apt-get install -y nodejs

## POSTGRESQL
sudo apt-get install -y postgresql postgresql-client postgresql-contrib
sudo apt-get install -y pgadmin3 postgresql-doc
# sudo -u postgres psql postgres
sudo apt install inotify-tools -y
