#!/bin/bash

sudo add-apt-repository ppa:morphis/anbox-support
sudo apt install anbox-modules-dkms
sudo modprobe ashmem_linux
sudo modprobe binder_linux
IPCs=ls -l /dev/{ashmem,binderfs}
if [ $? = 0 ]; then
    sudo apt install android-tools-adb
    sudo apt install wget lzip unzip squashfs-tools

    sudo snap install --devmode --beta anbox
    sudo apt install openjdk-11-jdk
fi

echo "Should GAPPS be installed? [y/n]"
wget https://raw.githubusercontent.com/geeks-r-us/anbox-playstore-installer/master/install-playstore.sh
chmod +x install-playstore.sh
sudo ./install-playstore.sh