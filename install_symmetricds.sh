#!/bin/bash

_check_system () {
    if [ -r /etc/os-release ]; then
        source /etc/os-release
        echo "  └── ✅ Os details found"; sleep 0.5;
        DISTRO=$ID
        echo "  └── ✅ Distro set to '$DISTRO'"; sleep 0.5;
        return 0
    else
        echo "  └── ❌ Os details can't be read"; sleep 0.5;
        echo "  └── ❌ Retrying with sudo"; sleep 0.5;
        sudo _check_system
        return 1
    fi
}

_ubuntu_install_java () {
    which wget &> /dev/null
    [ $? = 1 ] && sudo apt-get install -y wget &> /dev/null
    java --version &> /dev/null
    if [ $? = 127 ]; then
        wget https://javadl.oracle.com/webapps/download/AutoDL?BundleId=241526_1f5b5a70bf22433b84d0e960903adac8 -O ~/jre.tar.gz

        sudo mkdir /usr/java &> /dev/null
        cd /usr/java/
        sudo tar zxfv ~/jre.tar.gz
        rm -rf ~/jre.tar.gz
    else
        echo "|> Java already installed!"
    fi
}

_centos_install_java () {
    which wget
    [ $? = 1 ] && sudo yum install -y wget sym_service&> /dev/null
    java --version &> /dev/null
    if [ $? = 127 ]; then
        sudo yum update -y 
        sudo yum install -y unzip java-11-openjdk
    else
        echo "|> Java already installed!"
    fi
}
 

_install_symmetric () {
    wget -q https://liquidtelecom.dl.sourceforge.net/project/symmetricds/symmetricds/symmetricds-3.11/symmetric-server-3.11.7.zip -O ~/symmetric-server.zip
    # cp ~/Downloads/symmetric-server.zip ~/symmetric-server.zip

    sudo unzip -q -o ~/symmetric-server.zip -d ~/sbin/
    local INSTALL_PATH=~/sbin/symmetric-server-3.11.7

    export PATH="$PATH:$INSTALL_PATH/bin"
    tail ~/.bashrc | grep symmetric &> /dev/null
    [[ $? = 1 ]] && echo 'export PATH="$PATH:$INSTALL_PATH/bin"' >> ~/.bashrc || echo "|> Path already set.."
    echo "|> Setting up symmetric to run as a service on boot..."
    sudo $INSTALL_PATH/bin/sym_service install &> /dev/null
    echo "|> Starting up symmetric-server..."
    sudo $INSTALL_PATH/bin/sym_service start &> /dev/null
    [[ $? = 0 ]] && echo "|> Symmetric-server started successfully..." || echo "|> Symmetric-server already running..."
}


echo "Installing Prequisites..."
echo "|> Checking host machine..."
_check_system

echo "|> Installing java for $DISTRO..."
case $DISTRO in 
    ubuntu)        
        _ubuntu_install_java
        ;;

    centos)
        _centos_install_java
        ;;

    *)
        echo "|> Unkown os-distribution!"
        exit
        ;;
esac
if [ $? = 0 ]; then
echo "|> Java successfully installed..."
echo "|>--------------------------------------------------------<|"
java --version
echo "|>--------------------------------------------------------<|"
echo ""
else
echo "|> Java was not installed!"
exit
fi

echo "|> Install SymmetricDS...."
_install_symmetric
