#!/bin/bash

# ## Install apache
# sudo apt update && sudo apt upgrade
# sudo apt install apache2

# # start apache web server
# sudo sysetemctl status apache2

# # checking firewall
# if [ $(which ufw)=0 ]; do
#     echo "UFW firewall found, opening ports 80 (http) and 443 (https).."
#     sudo ufw allow 'Apache Full'
#     sudo ufw status
#     echo "Visit http://localhost/ to confirm apache2 is running..."

# fi

## CENTOS
# Install Epel repo
sudo yum update 
sudo yum -y install epel-release

# install Nginx
sudo yum -y install Nginx

# start nginx service
sudo sysetemctl start nginx
sudo sysetemctl enable nginx

# install net-tools :[optional]
sudo yum -y install net-tools
netstat -plntu

## INSTALLING LARAVEL
# Ensure php is installed >=5.6.4
#Install the webtatic repo
rpm -Uvh https://mirror.webtatic.com/yum/el7/webtatic-release.rpm