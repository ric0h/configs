#!/bin/bash

sudo apt update -y
sudo apt install -y \
build-essential \
manpages-dev

gcc --version

sudo apt install -y \
software-properties-common
sudo add-apt-repository ppa:ubuntu-toolchain-r/test
sudo apt install gcc-9 g++-9

sudo update-alternatives --install /usr/bin/gcc gcc /usr/bin/gcc-9 90 --slave /usr/bin/g++ g++ /usr/bin/g++-9 --slave /usr/bin/gcov gcov /usr/bin/gcov-9


sudo update-alternatives --config gcc

