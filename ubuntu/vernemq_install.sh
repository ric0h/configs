#!/bin/bash

#Download vernemq
cd $HOME/Downloads
wget https://github.com/vernemq/vernemq/releases/download/1.10.1/vernemq-1.10.1.bionic.x86_64.deb
sudo dpkg -i vernemq-1.10.1.bionic.x86_64.deb

sudo vernemq chkconfig
sudo systemctl start vernemq
sudo systemctl enable vernemq