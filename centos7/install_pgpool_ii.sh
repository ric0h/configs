#!/bin/bash
# Intstalling PgPool ii
sudo yum install -y wget

wget -O ~/pgpool_ii.rpm https://www.pgpool.net/yum/rpms/4.0/redhat/rhel-7-x86_64/pgpool-II-pg10-4.0.0-1pgdg.rhel7.x86_64.rpm

sudo yum --nogpgcheck localinstall -y pgpool_ii.rpm
    
rm -rf ~/pgpool_ii.rpm

# Configuring
# sample config file found at /usr/local/etc/pgpool.conf.sample

cp /usr/local/etc/pgpool.conf.sample /usr/local/etc/pgpool.conf
