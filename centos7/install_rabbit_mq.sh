#!/bin/bash

# Installs RabbitMQ server on a Cento7 running machine.

echo "Setting up the system..."
echo "|> Updating the system..."
sudo yum -y update &> /dev/null && echo "|> System update complete!"

if [ $? != 0 ]; then
    echo "|> System update failed!!"
    exit
fi


echo "Installing prequisites..."
echo "|> Installing erlang..."
echo "|> Downloading erlang package..."
sudo yum install -y epel-release wget &> /dev/null
wget https://packages.erlang-solutions.com/erlang-solutions-1.0-1.noarch.rpm &> /dev/null
sudo rpm -Uvh erlang-solutions-1.0-1.noarch.rpm &> /dev/null
sudo yum install -y esl-erlang &> /dev/null

if [ $? != 0 ]; then
    echo "|> Erlang installation failed!"
    exit
fi

echo "Installing RabbitMQ..."
echo "|> Downloading the latest RabbitMQ package..."
wget https://github.com/rabbitmq/rabbitmq-server/releases/download/v3.8.2/rabbitmq-server-3.8.2-1.el7.noarch.rpm &> /dev/null && echo " Download complete!"

if [ $? != 0 ]; then
    echo "|> Failed to download package!!"
    exit
fi

echo "|> Adding verification keys..."
rpm --import https://www.rabbitmq.com/rabbitmq-release-signing-key.asc &> /dev/null && echo "|>  Done!"
echo "|> Installing RabbitMQ..."
sudo yum install -y rabbitmq-server-*.noarch.rpm &> /dev/null  
if [ $? = 0 ]; then 
    echo "|> Done installing RabbitMQ!"
else
    echo "|> Failed to install RabbitMQ!!"
    exit
fi

echo "For further configuration and management, checkout > https://www.digitalocean.com/community/tutorials/how-to-install-and-manage-rabbitmq#managing-rabbitmq"