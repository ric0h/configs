#!/bin/bash

# update the system 
echo -ne "Updating system...\r"; sleep 1
sudo yum update -y &> /dev/null

# install apache
echo -ne "Installing apache...\r"; sleep 1
sudo yum install httpd &> /dev/null

# start and enable apache service
echo -ne "Starting up Apache service...\r"; sleep 1
sudo systemctl start httpd
sudo systemctl enable httpd

echo "Verify if Apache Service is running with \n 'sudo systemctl enable http'"

systemctl status httpd | grep running &> /dev/null
if [ $? = 0]; then 
    echo -ne "Apache service is running..."; sleep 1
fi

# enable apache on http ports 80 & 443
echo -ne "Enabling http service on firewall for ports 80 & 443../r"; sleep 1
sudo firewall-cmd --permanent --add-service=http &> /dev/null
sudo firewall-cmd --reload &> /dev/null

echo "Please configure your system as directed in this post\n https://phoenixnap.com/kb/install-apache-on-centos-7"
