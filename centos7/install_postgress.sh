#!/bin/bash

# This script install postgresql database version 11.
# This script works on centos7

# Add the official postgresql repository
sudo yum install -y https://download.postgresql.org/pub/repos/yum/reporpms/EL-7-x86_64/pgdg-redhat-repo-latest.noarch.rpm

# Install Postgresql from CentOs Repositories
sudo yum install -y postgresql11-server postgresql11
sudo rpm -qi postgresql11-server

# Activate the Postgresql database cluster
sudo /usr/pgsql-11/bin/postgresql-11-setup initdb

# Enabling postgresql
sudo systemctl start postgresql-11
sudo systemctl enable postgresql-11

# Enable firewall
sudo firewall-cmd --add-service=postgresql --permanent
sudo firewall-cmd --reload

## Edit the file /var/lib/pgsql/11/data/postgresql.conf and set Listen address to your server IP address or “*” for all interfaces.
#
# listen_addresses = '192.168.18.9'
#
# Also set PostgreSQL to accept remote connections
#
# $ sudo vim /var/lib/pgsql/11/data/pg_hba.conf
#
# # Accept from anywhere
#
# host all all 0.0.0.0/0 md5
#
# # Accept from trusted subnet
#
# host all all 192.168.18.0/24 md5
#
## Restart service
#
# sudo systemctl restart postgresql-11
#

# Configuring the Database
# Switching Over to the postgres Account
# Switch over to the postgres account on your server by typing:
#
#       sudo -i -u postgres
#
# You can now access a Postgres prompt immediately by typing:
#
#       psql
#
# # This will log you into the PostgreSQL prompt, and from here you are free to interact with the database management system right away.
#
# Exit out of the PostgreSQL prompt by typing:
#
#       \q
# # This will bring you back to the postgres Linux command prompt. Now return to your original sudo account with the following:
#
#       exit

# Change default user password
sudo -u postgres psql -U postgres -c "alter user postgres with password '334f560113b6e1c6c841dca856065b79'"
sudo -u postgres psql -d template1 -U postgres -c "create user ally with password '334f560113b6e1c6c841dca856065b79'"
sudo -u postgres psql -U postgres -c "alter role ally with createdb"
