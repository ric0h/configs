#!/bin/bash

# This script installs a specific version of haproxy on centos7, 
# the version is passed as an argument when calling the script.
# in case no version is provided, the script defaults to version $HAPROXY_VERSION.
# todo: Check the arguments passed include a version.
HAPROXY_VERSION=2.1
version=2.1.3
# Install Prerequisites
echo "|> Installing Prequisites..."
sudo yum install -y gcc tar openssl-devel readline-devel systemd-devel make pcre-devel &> /dev/null && echo "|> Prequisites Installed!"

if [ $? != 0 ]; then 
    echo "|> Failed in installing prequistes..."
    exit 
fi

echo "|> Downloading haproxy..."
cd $HOME
wget http://www.haproxy.org/download/$HAPROXY_VERSION/src/haproxy-$version.tar.gz -O ~/haproxy.tar.gz &> /dev/null && echo "|> Download Complete!"

if [ $? != 0 ]; then 
    echo "|> Failed to download haproxy-$version.tar.gz..."
    exit 
fi

echo "|> Extracting archive..."
tar xzvf ~/haproxy.tar.gz -C $HOME/ &> /dev/null && echo "|> Extraction done!"

echo "|> Compiling haproxy..."
cd $HOME/haproxy-$version
make TARGET=linux-glibc &> /dev/null && echo "|> Done Compiling glibc"
sudo make install &> /dev/null && echo "|>  Done Compiling haproxy package"

if [ $? != 0 ]; then 
    echo "|> Failed in compiling package..." 
    exit 
fi

echo "|> Setting up Haproxy..."
sudo mkdir -p /etc/haproxy
sudo mkdir -p /var/lib/haproxy 
sudo touch /var/lib/haproxy/stats
sudo ln -s /usr/local/sbin/haproxy /usr/sbin/haproxy &> /dev/null
sudo cp $HOME/haproxy-$version/examples/haproxy.init /etc/init.d/haproxy &> /dev/null
sudo chmod 755 /etc/init.d/haproxy
sudo systemctl enable haproxy
sudo systemctl daemon-reload
sudo chkconfig haproxy on
sudo useradd -r haproxy &> /dev/null

echo "|> Successfully setup haproxy!"
echo "
==================================================="
haproxy -v
echo "====================================================
"

echo "|> Enabling Firewall Rules..."
sudo firewall-cmd --permanent --zone=public --add-service=http &> /dev/null
sudo firewall-cmd --permanent --zone=public --add-port=8181/tcp &> /dev/null
sudo firewall-cmd --reload &> /dev/null

echo "|> Head out to this link for configuring Haproxy --> https://upcloud.com/community/tutorials/haproxy-load-balancer-centos/#point-3"


