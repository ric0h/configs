#!/bin/bash
## Using Asdf to manage versions
# Ref: https://asdf-vm.com/#/

# Add elixir versions as found when `asdf list all elixir`
ERLANG_VERSION=25.3.2.6
ELIXIR_VERSION=1.15.6-otp-25

# Other build envrionment variables as required
export KERL_BUILD_DOCS=yes
KERL_INSTALL_MANPAGES=no
KERL_INSTALL_HTMLDOCS=no

# Utils
print_loader() {
  echo -ne "$1 .  \r"
  sleep 0.3
  echo -ne "$1 .. \r"
  sleep 0.3
  echo -ne "$1 ...\r"
  sleep 0.4
  return 0
}

loading() {
  local start="$SECONDS"
  local end
  local task_name
  local task_pid="$!"
  if [[ -n "$1" ]]; then
    task_name="$1"
  else
    task_name=""
  fi

  while [ "$(ps a | awk '{print $1}' | grep $task_pid)" ]; do
    end="$SECONDS"
    print_loader "$task_name \t[$((end - start)) secs]"
  done
  end="$SECONDS"
  echo -ne "[$task_pid : $task_name][$((end - start)) secs] DONE!\n\r"
}

# Lookup distro version
distro_version() {
  data=$(cat /etc/*-release | grep 'ID_LIKE')
  local distro

  if [[ "$data" == *"debian"* ]]; then
    distro="debian"
  elif [[ "$data" == *"rhel"* ]]; then
    distro="rhel"
  fi

  if [[ -z "$distro" ]]; then
    echo -e "Error ascertaining the correct distro.\n Got $data"
    exit 1
  else
    export TMP_DISTRO=$distro
    return 0
  fi
}

# Setup dependencies
rhel_setup_dependencies() {
  sudo yum update -y 1>>./versioned_elixir_install.log
  sudo yum install -y \
    automake autoconf libreadline-dev \
    libncurses-dev libssl-dev libyaml-dev \
    libxslt-dev libffi-dev libtool unixodbc-dev \
    unzip curl \
    build-essential autoconf m4 libncurses5-dev \
    libwxgtk3.0-dev libgl1-mesa-dev libglu1-mesa-dev \
    libpng-dev libssh-dev unixodbc-dev xsltproc fop \
    git openssl-devel ncurses-devel 1>>./versioned_elixir_install.log
}

deb_setup_dependencies() {
  sudo apt-get update -y 1>>./versioned_elixir_install.log
  sudo apt-get upgrade 1>>./versioned_elixir_install.log
  sudo apt-get install -y \
    build-essential autoconf \
    m4 \
    libncurses5-dev \
    libwxgtk-webview3.0-gtk3-dev libwxgtk3.0-gtk3-dev libwxgtk3.0-gtk3-0v5 libgl1-mesa-dev libglu1-mesa-dev libpng-dev \
    libssh-dev \
    unixodbc-dev \
    xsltproc fop \
    openjdk-11-jdk \
    libxml2-utils \
    automake libreadline-dev \
    libssl-dev libyaml-dev \
    libxslt-dev libffi-dev libtool \
    unzip curl git 1>>./versioned_elixir_install.log

  # sudo apt-get install -y \
  #   automake autoconf libreadline-dev \
  #   libncurses-dev libssl-dev libyaml-dev \
  #   libxslt-dev libffi-dev libtool unixodbc-dev \
  #   unzip curl \
  #   build-essential autoconf m4 libncurses5-dev \
  #   libwxgtk3.0-dev libgl1-mesa-dev libglu1-mesa-dev \
  #   libpng-dev libssh-dev unixodbc-dev xsltproc fop \
  #   git openssl-devel ncurses-devel 1>>./versioned_elixir_install.log
}

setup_dependencies() {
  distro_version

  if [[ "$TMP_DISTRO" == "rhel" ]]; then
    rhel_setup_dependencies
  elif [[ "$TMP_DISTRO" == "debian" ]]; then
    deb_setup_dependencies
  fi

}

# Setup asdf if not setup
setup_asdf() {
  asdf --version 1>>./versioned_elixir_install.log
  if [ "$?" != 0 ]; then
    git clone https://github.com/asdf-vm/asdf.git ~/.asdf 1>>./versioned_elixir_install.log
    if [ "$?" != 0 ]; then
      cat ~/.bashrc | grep ". $HOME/.asdf/asdf.sh"
      if [ $? -eq 1 ]; then
        echo -e '\n. $HOME/.asdf/asdf.sh' >>~/.bashrc
        echo -e '\n. $HOME/.asdf/completions/asdf.bash' >>~/.bashrc
        source ~/.bashrc
      fi

    else
      exit 1
    fi
  fi
}

setup_erlang() {
  local start=$SECONDS
  . ~/.bashrc

  # install asdf plugins
  asdf plugin add erlang &>>./versioned_elixir_install.log

  # install specified versions
  asdf install erlang $ERLANG_VERSION &>>./versioned_elixir_install.log

  # Setup installed versions
  asdf global erlang $ERLANG_VERSION
  . ~/.bashrc

  local duration=$((SECONDS - start))
  return $duration
}

setup_elixir() {
  . ~/.bashrc

  # install asdf plugins
  asdf plugin add elixir &>>./versioned_elixir_install.log

  # install specified versions
  asdf install elixir $ELIXIR_VERSION &>>./versioned_elixir_install.log

  # Setup installed versions
  asdf global elixir $ELIXIR_VERSION

  . ~/.bashrc
}

setup_dependencies &
loading "Setting up dependencies"

setup_asdf &
loading "Setting up ASDF"

echo -e """
  Checking build docs variables
  ----------------------------
  KERL_BUILD_DOCS: $KERL_BUILD_DOCS
  KERL_INSTALL_MANPAGES: $KERL_INSTALL_MANPAGES
  KERL_INSTALL_HTMLDOCS: $KERL_INSTALL_HTMLDOCS
  ----------------------------
  """

setup_erlang &
loading "Installing erlang"

setup_elixir &
loading "Installing elixir"
