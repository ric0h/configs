#!/bin/bash
## variables set in this script
# all the variables as found in the `/etc/os-release` folder.
# INSTALLED_FIREWALL
# DISTRO
# INSTALLED_ELIXIR_VERSION, ELIXIR_COMPILATION_OTP, INSTALLED_OTP_VERSION,
# INSTALLED_NODE_VERSION, INSTALLED_NODE_VERSION, INSTALLED_NPM_VERSION
# INSTALLED_IMAGEMAGICK_VERSION, INSTALLED_GIT_VERSION

# system running on
# Loads the /etc/os-release file to the enviroment
# looks up the ID value from the file and assigns it to DISTRO var
_check_system () {
    if [ -r /etc/os-release ]; then
        source /etc/os-release
        echo "  └── ✅ Os details found"; sleep 0.5;
        DISTRO=$ID
        echo "  └── ✅ Distro set to '$DISTRO'"; sleep 0.5;
        return 0
    else
        echo "  └── ❌ Os details can't be read"; sleep 0.5;
        echo "  └── ❌ Retrying with sudo"; sleep 0.5;
        sudo _check_system
        return 1
    fi
}

# elixir and erlang version
_check_elixir_erlang () {
    elixir -v &> /dev/null # check if elixir is installed
    if [ $? = 0 ]; then # if installed, verify installation assigning versions to vars
        local check_elixir_version=$(elixir -v | grep Elixir ) # get elixir version
        local check_erlang_version=$(elixir -v | grep Erlang/OTP) # get otp version of erlang
        
        declare -a ex_version
        declare -a erl_version
        ex_version=( $check_elixir_version ) # extract elixir version number
        erl_version=( $check_erlang_version ) # extract erlanfs otp version number
        
        # assing installed versions to vars
        INSTALLED_ELIXIR_VERSION=${ex_version[1]}
        ELIXIR_COMPILATION_OTP=$(echo ${ex_version[5]} | tr ")" "\n")
        INSTALLED_OTP_VERSION=${erl_version[1]}

        # update user
        echo "  └── ✅ Elixir version: $INSTALLED_ELIXIR_VERSION compiled with OTP: $ELIXIR_COMPILATION_OTP"; sleep 0.5;
        echo "  └── ✅ Erlang & OTP version: $INSTALLED_OTP_VERSION"; sleep 0.5;

        # validate if elixir is compiled with same otp as installed
        # get otp used to compile elixir        
        if [ $ELIXIR_COMPILATION_OTP -eq $INSTALLED_OTP_VERSION ]; then
            echo -ne "      └── ✅ OTP is a match!\r"; sleep 2;
        else
            echo "  └── ❌ OTP versions DONT match please check your elixir installation."; sleep 0.5;
            return 1
        fi
        return 0
    else
        echo "  └── ❌ Elixir is NOT installed." 
        return 1
    fi
}

# check npm version
_check_npm_nodejs () {
    # check if nodejs exists
    local success=0
    nodejs -v &> /dev/null
    if [ $? = 0 ]; then
        INSTALLED_NODE_VERSION=$(nodejs -v)
        echo "  └── ✅ Nodejs version: $INSTALLED_NODE_VERSION"; sleep 0.5;
    elif [ node -v &> /dev/null ] && [ $? = 0 ]; then
        INSTALLED_NODE_VERSION=$(node -v)
    else
        echo "  └── ❌ Nodejs is NOT installed."; sleep 0.5;
        success=1
    fi
    
    # check if npm exists
    npm -v &> /dev/null
    if [ $? = 0 ] && [ -n $INSTALLED_NODE_VERSION  ]; then
        INSTALLED_NPM_VERSION=$(npm -v)
        echo "  └── ✅ Npm version: $INSTALLED_NPM_VERSION"
    else
        echo "  └── ❌ NPM in NOT installed."
        success=1
    fi
}

# check imagemagive version
_check_convert_imagemagick () {
    which convert &> /dev/null
    if [ $? -eq 0 ]; then
        local convert=$(convert -v | grep Version)
        declare -a imagemagick
        imagemagick=( $convert )
        INSTALLED_IMAGEMAGICK_VERSION=${imagemagick[2]}
        echo "  └── ✅ ImageMagick version: $INSTALLED_IMAGEMAGICK_VERSION"; sleep 0.5;
        return 0
    else
        echo "  └── ❌ ImageMagick NOT installed."; sleep 0.5;
        return 1
    fi
}

# check the installed firewall
_check_firewall () {
    which firewall-cmd &> /dev/null
    if [ $? -eq 0 ]; then
        INSTALLED_FIREWALL='firewall-cmd'
        FIREWALL_VERSION=$(sudo firewall-cmd --version)
        echo "  └── ✅ $INSTALLED_FIREWALL version: $FIREWALL_VERSION"

    elif [[ $(which ufw &> /dev/null) ]]; then
        FIREWALL_VERSION=$(sudo ufw --version)
        INSTALLED_FIREWALL='ufw'
        echo "  └── ✅ $INSTALLED_FIRWALL version: $FIREWALL_VERSION"
        return 0
    else
        echo "  └── ❌ Install firewall tool."
        return 1
    fi
}

# check for git version
_check_git_version () {
    git --version &> /dev/null
    if [ $? -eq 0 ]; then
        declare -a git
        git=( $(git --version) )
        INSTALLED_GIT_VERSION=${git[2]}
        echo "  └── ✅ Git version: $INSTALLED_GIT_VERSION"; sleep 0.5;
        return 0
    else
        echo "  └── ❌ Git Not installed."; sleep 0.5;
        return 1
    fi
}

# OTHER ENV_VARS
# function to validate a given varible name is not nil
_check_env_var () {
    # echo -ne "\033[2K"
    local value=$1
    [[ ! -z $value ]] && return 0 || return 1
}

# function to load vars from the default env file at users home
_default_env_file? () {
    local default_file
    if [ -z $ALLY_ENV ]; then default_file=$HOME/.ally.env;
    else default_file=$ALLY_ENV
    fi
    if [ -f $default_file ]; then 
        echo "  └── ✅ Default Ally_env file '$default_file' found."; sleep 0.4;
        ALLY_ENV=$default_file
        sleep 0.3
        source $default_file &> /dev/null 
        return $?
    else echo -e "  └── ❌ default '~/.ally.env' file NOT found."
        sleep 0.3
        return 1
    fi
}

# function to user the values in a given file
_set_env_file () {
    if [ -s $1 ]; then 
    echo "  └── ✅ Found custom env file at $1"
    source $1 &> /dev/null
    return $?
    else  echo " └── ❌ $1 NOT a valid env file."; return 1;
    fi
}

# MAIN EXECUTION
# function to check all installed dependencies
_installed_deps () {
    local success=0
    _check_system; if [ $? = 1 ]; then success=1; fi;
    _check_firewall; if [ $? = 1 ]; then success=1; fi;
    _check_git_version; if [ $? = 1 ]; then success=1; fi;
    _check_elixir_erlang; if [ $? = 1 ]; then success=1; fi;
    _check_npm_nodejs; if [ $? = 1 ]; then success=1; fi;
    return $success
}

# ENV VARIABLES
# function checks all production database variables
_check_server_vars () {
    _check_env_var $PORT; if [ $? = 1 ]; then echo "     └── ❌ \$PORT is not set"; exit; fi;
    _check_env_var $HOST; if [ $? = 1 ]; then echo "     └── ❌ \$HOST is not set"; exit; fi;
    _check_env_var $KEY_FILE; if [ $? = 1 ]; then echo "     └── ❌ \$KEY_FILE is not set"; exit; fi;
    _check_env_var $CERT_FILE; if [ $? = 1 ]; then echo "     └── ❌ \$CERT_FILE is not set"; exit; fi;
    _check_env_var $SECRET_KEY_BASE; if [ $? = 1 ]; then echo "     └── ❌ \$SECRET_KEY_BASE is not set"; exit; fi;
    return $success
}

_check_prod_db_vars () {
    local success=0
    # check production database env vars
    _check_env_var $PROD_DB; if [ $? = 1 ]; then exit; fi;
    _check_env_var $PROD_DB_HOSTNAME; if [ $? = 1 ]; then exit; fi;
    _check_env_var $PROD_DB_USERNAME; if [ $? = 1 ]; then exit; fi;
    _check_env_var $PROD_DB_PASSWORD; if [ $? = 1 ]; then exit; fi;
    return $success
}

# checks that all the environment varibles are set
_validate_env () {
    local success=0
    # check production database env vars
    _check_prod_db_vars; if [ $? = 1 ]; then exit; fi;
    _check_server_vars; if [ $? = 1 ]; then exit; fi;

}

# Check build_dependencies
echo "└── Checking installed dependencies..."; 
_installed_deps
if [ $? = 0 ]; then echo -e "└── 💯 Dependencies set";
else echo "⛔️ Please Install specified dependencies"; exit
fi

# Load default env file
echo "└──  Checking environment variables";
_default_env_file?
if [ $? = 0 ]; then echo "  └── ✅ env file '$ALLY_ENV' is set. ";
else 
    echo "  └── ⛔️ Failed to set default ally env"
    sleep 0.5
    read -p "  └── Please enter absolute path to ally.env: " ally_env_file
    _set_env_file $ally_env_file
    if [ $? = 0 ]; then echo "  └── ✅ env file '$ally_env_file' is set."; sleep 0.5;
    else echo "  └── ⛔️ File unreadable, please check with sys_admin"; sleep 0.5;
    exit
    fi
fi

# Check SERVER_SETTINGS 
_validate_env
    