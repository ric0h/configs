#!/bin/bash
update_info () {
    echo -ne 'Enter the required details detailed here below, you can chose the\n name you need to choose by picking each answer correctly. \nThis sets the values of the configurations we are using, please confirm that the value chosen is what you intend to user otherwise there might be some inconsistenies.\r'

    if [ $VAL1 = 0 ]; then 
        EX_V=$FALSE 
    else 
        EX_V=$TRUE 
    fi
    
    if [ $VAL2 = 0 ]; then
        PORT=$FALSE 
    else
        PORT=$TRUE
    fi

    if [ $VAL3 = 0 ]; then 
        P_DB=$FALSE 
    else 
        P_DB=$TRUE 
    fi

    echo -ne "\n$EX_V Elixir version\n$PORT API port\n$P_DB Production Database.\n\r"
    return 0
}

for (( DONE=0; $DONE < 10; DONE++ )); do
    echo -ne "Processing\r"; sleep 1;
    echo -ne "Processing.\r"; sleep 1;
    echo -ne "Processing..\r"; sleep 1;
    echo -ne "Processing...\r"; sleep 1;
    echo -ne "Processing....\r"; sleep 1;
    echo -ne "                      \r";
done
echo -ne '#__________(10%)\r'; 
echo -ne '##_________(20%)\r';  
echo -ne '###________(30%)\r'; 
echo -ne '####_______(40%)\r';  
echo -ne '#####______(50%)\r';  
echo -ne '######_____(60%)\r';  
echo -ne '#######____(70%)\r';  
echo -ne '########___(80%)\r';  
echo -ne '#########__(90%)\r';  
echo -ne '##########_(100%%)'; 
echo 'Thanks for waiting!'

TRUE=✅
FALSE=❌
VAL1=0
VAL2=0
VAL3=0
VAL4=0
update_info


read -p "Please enter the elixir version to build the application: " ev_v
if [ $ev_v = "1.9.4" ]; then 
    VAL1=1
    update_info
else
    update_info
fi

