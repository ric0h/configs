#!/bin/bash

flatten_ext () {
    if [ ! -d $TARGET_DIR/$1 ]; then 
        echo " dir $TARGET_DIR/$1 does not exist, creating..."
        mkdir $TARGET_DIR/$1
    fi    
    find $WORKING_DIR -name "*.$1" -exec mv '-fv' '{}' "$TARGET_DIR/$1/" ";"
    return $?
}

clean_empty_dirs () {
    rmdir $WORKING_DIR/**/**/*/
    rmdir $WORKING_DIR/**/*/
    rmdir $WORKING_DIR/*/
    return 0
}

setting_dirs () {
    echo "Got $1 args.."
    echo "...please confirm '$2' is the dir with subdirs "
    echo "...and '$3' is your target dir."
    read -p "...confirm? [y/n]" confirmation
    
    if [  $confirmation = 'y' ]; then
        echo "Thanks for confirming"
        WORKING_DIR=$2
        TARGET_DIR=$3
        return 0
    else
        echo "Failed to set working dirs"
        return 1
    fi

}

set_extensions() {
    echo "Enter the extensions you wish to preserve."
    read -p "without the '.' and seperated with whitespaces: " exts

    EXTS=$(echo $exts | tr " " "\n")
}

if [ $# -gt 1 ]; then
    setting_dirs $# $1 $2
    set_extensions

    for i in $EXTS; do
        flatten_ext $i
    done
    clean_empty_dirs 
else
    echo "Provide dir with subdirs and target dir"
fi
