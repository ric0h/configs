#!/bin/bash

## Updating system
sudo apt-get install apt-transport-https curl gpg -y
sudo apt-get update && sudo apt-get upgrade -y

## GIT CONFIG
GIT_USER="ricoh"
GIT_EMAIL="moshericoe@gmail.com"

## Add repos
# Chrome
wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | sudo apt-key add -
sudo sh -c 'echo "deb http://dl.google.com/linux/deb/ stable main" > /etc/apt/sources.list.d/chrome.list'

# Git
sudo add-apt-repository ppa:git-core/ppa -y

# VS Code
curl https://packages.microsoft.com/keys/microsoft.asc | gpg --dearmor > packages.microsoft.gpg
sudo install -o root -g root -m 644 packages.microsoft.gpg /usr/share/keyrings/ -y
sudo sh -c 'echo "deb [arch=amd64 signed-by=/usr/share/keyrings/packages.microsoft.gpg] https://packages.microsoft.com/repos/vscode stable main" > /etc/apt/sources.list.d/vscode.list'

## Setting up environment for development
# todo: add check of different systems
sudo apt-get install -y build-essential git vim code google-chrome-stable

# GIT SETUP
git config --global user.name $GIT_USER
git config --global user.email $GIT_EMAIL
git config --global core.editor vim