#!/bin/bash
echo "======================================================================"
echo "|                 Cleaning Unnecessary dependencies                  |"
echo "======================================================================"

sudo apt autoremove

echo "==================Clearing thumbnails cache==========================="
du -sh ~/.cache/thumbnails)
sudo rm -rf ~/.cache/thumbnails
